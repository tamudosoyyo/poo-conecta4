import java.util.Scanner;

public interface ModosJuego {

    public static void jugarModoBasico(ControladorJuego controlador, Scanner sc) {
        while (!controlador.getJuego().hayGanador() && !controlador.getJuego().hayEmpate()) {
            System.out.println("Tablero actual:");
            System.out.println(controlador.getJuego().getTablero().toString());
            controlador.getJuego().getJugadorActual().jugarTurno(controlador, sc);

            if(!controlador.getJuego().hayGanador()){
                controlador.getJuego().cambiarJugadorActual();
            }
        }
    
        if (controlador.getJuego().hayGanador()) {
            System.out.println("El ganador es el jugador: " + controlador.getJuego().getJugadorActual().getNombre()); 
        } else {
            System.out.println("La partida ha terminado en empate.");
        }
    }

    public static void jugarModoEntrenamiento(ControladorJuego controladorhumanoconmaquina, Scanner sc) {
        // Implementación del modo de juego entrenamiento
        while (!controladorhumanoconmaquina.getJuego().hayGanador() && !controladorhumanoconmaquina.getJuego().hayEmpate()) {
            // Turno del humano
            System.out.println("Tablero actual:");
            System.out.println(controladorhumanoconmaquina.getJuego().getTablero().toString());
            controladorhumanoconmaquina.getJuego().getJugadorActual().jugarTurno(controladorhumanoconmaquina, sc);

            // Turno de la máquina
            if(!controladorhumanoconmaquina.getJuego().hayGanador()){
                controladorhumanoconmaquina.getJuego().cambiarJugadorActual();
                controladorhumanoconmaquina.getJuego().getJugadorActual().jugarTurno(controladorhumanoconmaquina, sc);
                System.out.println("Movimiento de la maquina:");
                System.out.println(controladorhumanoconmaquina.getJuego().getTablero().toString());
            }
            if(!controladorhumanoconmaquina.getJuego().hayGanador()){
                controladorhumanoconmaquina.getJuego().cambiarJugadorActual();
            }
        }

        if (controladorhumanoconmaquina.getJuego().hayGanador()) {
            System.out.println("El ganador es el jugador: " + controladorhumanoconmaquina.getJuego().getJugadorActual().getNombre()); 
        } else {
            System.out.println("La partida ha terminado en empate.");
        }
    }    

    public static void jugarModoDemo(ControladorJuego controladormaquinas, Scanner sc) {
        while (!controladormaquinas.getJuego().hayGanador() && !controladormaquinas.getJuego().hayEmpate()) {
            System.out.println("Tablero actual:");
            System.out.println(controladormaquinas.getJuego().getTablero().toString());
            controladormaquinas.getJuego().getJugadorActual().jugarTurno(controladormaquinas, sc);

            if(!controladormaquinas.getJuego().hayGanador()){
                controladormaquinas.getJuego().cambiarJugadorActual();
            }
        }
    
        if (controladormaquinas.getJuego().hayGanador()) {
            System.out.println("Tablero finalizado:");
            System.out.println(controladormaquinas.getJuego().getTablero().toString());
            System.out.println("El ganador es el jugador: " + controladormaquinas.getJuego().getJugadorActual().getNombre()); 
        } else {
            System.out.println("La partida ha terminado en empate.");
        }
    }
}
