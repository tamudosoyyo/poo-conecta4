public class Ficha {
    private int fila;
    private int columna;
    private String color;

    public Ficha(String color) {
        this.color = color;
    }

    
    public void setFila(int fila) {
        this.fila = fila;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }

    public String getColor() {
        return color;
    }
}


