import java.util.Stack;

public class ControladorJuego {
    private Juego juego;
    private Stack<Movimiento> pilaDeshacer;
    private Stack<Movimiento> pilaRehacer;

    public ControladorJuego(Jugador jugador1, Jugador jugador2) {
        juego = new Juego(jugador1, jugador2);
        pilaDeshacer = new Stack<>();
        pilaRehacer = new Stack<>();
    }

    public Juego getJuego() {
        return this.juego;
    }

    public void realizarJugada(int columna) throws ColumnaLlenaException {
        // Realizar la jugada en el tablero
        Ficha ficha = new Ficha(juego.getJugadorActual().getColor());
        juego.colocarFicha(columna, ficha);
        Movimiento movimiento = new Movimiento(columna, ficha);
        pilaDeshacer.push(movimiento);
        pilaRehacer.clear();
    }

    public void deshacerJugada() {
        if (!pilaDeshacer.isEmpty()) {
            Movimiento movimiento = pilaDeshacer.pop();
            movimiento.deshacer(juego.getTablero());
            pilaRehacer.push(movimiento);
        }
    }

    public void rehacerJugada() {
        if (!pilaRehacer.isEmpty()) {
            Movimiento movimiento = pilaRehacer.pop();
            movimiento.rehacer(juego.getTablero());
            pilaDeshacer.push(movimiento);
        }
    }
}

