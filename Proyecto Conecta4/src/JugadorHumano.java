import java.util.Scanner;

public class JugadorHumano extends Jugador {
    public JugadorHumano(String color, String nombre) {
        super(color,nombre);
    }

    @Override 
    public void jugarTurno(ControladorJuego controlador, Scanner sc){
        System.out.println("Turno del jugador: " + controlador.getJuego().getJugadorActual().getNombre()); 
        int columna;
        do{
            System.out.println("Elige una columna (0-6):");
            columna = sc.nextInt();
            sc.nextLine();

            if(controlador.getJuego().getTablero().columnaLlena(columna)){
                System.out.println("La columna ya esta llena. Entiendo que seas humano y puedas equivocarte, te voy a dejar elegir otra.");
            }
        }while(controlador.getJuego().getTablero().columnaLlena(columna));

        try {
            controlador.realizarJugada(columna);
            System.out.println("Tablero en borrador:");
            System.out.println(controlador.getJuego().getTablero().toString());

            System.out.println("Si te has equivocado al seleccionar la posicion, puedes escribir 'deshacer'. De lo contrario, puedes escribir 'continuar'");
            String deshacer = sc.nextLine();
            if(deshacer.equals("deshacer")){
                controlador.deshacerJugada();
                System.out.println("Tablero en borrador:");
                System.out.println(controlador.getJuego().getTablero().toString());
                System.out.println("Si te has equivocado y no querías deshacer tu ultimo movimiento, puedes escribir 'rehacer'. De lo contrario, puedes escribir 'continuar'");
                String rehacer = sc.nextLine();
                if(rehacer.equals("rehacer")){
                    controlador.rehacerJugada();
                }
            }
        }catch (Exception e) {
            System.out.println("Ha ocurrido un error inesperado: " + e.getMessage());
        }
    };
}

