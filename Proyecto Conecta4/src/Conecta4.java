import java.util.Scanner;

public class Conecta4 implements ModosJuego{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ControladorJuego controlador = new ControladorJuego(new JugadorHumano("ROJO","Jugador1"), new JugadorHumano("AMARILLO","Jugador2"));
        ControladorJuego controladorhumanoconmaquina = new ControladorJuego(new JugadorHumano("ROJO","JugadorHumano"), new JugadorMaquina("AMARILLO","JugadorMaquina"));
        ControladorJuego controladormaquinas = new ControladorJuego(new JugadorMaquina("ROJO","JugadorMaquina1"), new JugadorMaquina("AMARILLO","JugadorMaquina2"));

        int opcion;

        // Mostrar menú de opciones
        System.out.println("Elige una de las opciones para jugar a Conecta4:");
        System.out.println("1. Jugar modo básico (jugador vs jugador)");
        System.out.println("2. Jugar modo entrenamiento (jugador vs maquina)");
        System.out.println("3. Jugar modo demo (maquina vs maquina)");
        System.out.println("4. Salir de Conecta4");

        opcion = sc.nextInt();
        sc.nextLine();

        switch (opcion) {
            case 1:
                ModosJuego.jugarModoBasico(controlador, sc);
                break;
            case 2:
                ModosJuego.jugarModoEntrenamiento(controladorhumanoconmaquina, sc);
                break;
            case 3:
                ModosJuego.jugarModoDemo(controladormaquinas, sc);
                break;
            case 4:
                break;
            default:
                System.out.println("No has introducido ninguna opcion valida");
                break;
        }
    }
}

