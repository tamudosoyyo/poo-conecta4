public class Juego {
    private Tablero tablero;
    private Jugador jugador1;
    private Jugador jugador2;
    private Jugador jugadorActual;

    public Juego(Jugador jugador1, Jugador jugador2) {
        tablero = new Tablero();
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        jugadorActual = this.jugador1;
    }

    public Jugador getJugador1() {
        return this.jugador1;
    }
    public Jugador getJugador2() {
        return this.jugador2;
    }

    public void colocarFicha(int columna, Ficha ficha) throws ColumnaLlenaException {
        if (tablero.insertarFicha(columna, ficha)) {
        } else {   
            throw new ColumnaLlenaException("No se ha podido insertar la ficha");
        }
    }

    public boolean hayGanador() {
        Ficha ficha = new Ficha(jugadorActual.getColor());
        return tablero.hayGanador(ficha);
    }

    public boolean hayEmpate() {
        return tablero.hayEmpate();
    }

    public Tablero getTablero() {
        return tablero;
    }

    public Jugador getJugadorActual() {
        return jugadorActual;
    }    
    public void cambiarJugadorActual(){
        if(this.jugadorActual == this.jugador1){
            this.jugadorActual = this.jugador2;
        }else{
            this.jugadorActual = this.jugador1;
        }
    }
    
}


