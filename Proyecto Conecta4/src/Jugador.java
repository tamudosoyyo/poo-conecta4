import java.util.Scanner;

public abstract class Jugador {
    private String color;
    private String nombre;

    public Jugador(String color, String nombre) {
        this.color = color;
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public String getNombre() {
        return nombre;
    }
    
    public abstract void jugarTurno(ControladorJuego controlador, Scanner sc);
}

