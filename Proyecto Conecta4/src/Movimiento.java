public class Movimiento {
    private int columna;
    private Ficha ficha;

    public Movimiento(int columna, Ficha ficha) {
        this.columna = columna;
        this.ficha = ficha;
    }

    public void deshacer(Tablero tablero) {
        tablero.eliminarFicha(ficha, columna);
    }

    public void rehacer(Tablero tablero) {
        try {
            tablero.insertarFicha(columna, ficha);
        } catch (Exception e) {
            System.out.println("Ha ocurrido un error inesperado: " + e.getMessage());
        }
    }
    
}


