public class Tablero {
    public static final int FILAS = 6;
    public static final int COLUMNAS = 7;
    public Casilla[][] casillas;

    public Tablero() {
        casillas = new Casilla[FILAS][COLUMNAS];
        for (int fila = 0; fila < FILAS; fila++) {
            for (int col = 0; col < COLUMNAS; col++) {
                casillas[fila][col] = new Casilla();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int fila = 0; fila < FILAS; fila++) {
            for (int col = 0; col < COLUMNAS; col++) {
                Ficha ficha = casillas[fila][col].getFicha();
                if (ficha == null) {
                    sb.append("-");
                } else {
                    sb.append(ficha.getColor().charAt(0));
                }
                sb.append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }


    public boolean insertarFicha(int columna, Ficha ficha) throws ColumnaLlenaException {
        if (columna < 0 || columna > COLUMNAS) {
            throw new IndexOutOfBoundsException();
        }
        int filaVacia = FILAS - 1;
        while (filaVacia >= 0 && casillas[filaVacia][columna].getFicha() != null) {
            filaVacia--;
        }
        if (filaVacia < 0) {
            throw new ColumnaLlenaException("La columna está llena");
        }
        casillas[filaVacia][columna].setFicha(ficha);
        ficha.setFila(filaVacia);
        ficha.setColumna(columna);
        return true;
    }

    
    public boolean columnaLlena(int columna) {
        for (int fila = 0; fila < 6; fila++) {
            if (casillas[fila][columna].getFicha() == null) {
                return false;
            }
        }
        return true;
    }
    

    public void eliminarFicha(Ficha ficha, int columna) {
        // Recorremos las filas desde arriba hacia abajo
        for (int fila = 0; fila < FILAS; fila++) {
            // Si encontramos la ficha, la eliminamos
            if (casillas[fila][columna].getFicha() != null &&
                casillas[fila][columna].getFicha().getColor().equals(ficha.getColor())) {
                casillas[fila][columna].setFicha(null);
                break;
            }
        }
    }

    public boolean hayEmpate() {
        // Comprobar si todas las casillas del tablero están ocupadas
        for (int fila = 0; fila < FILAS; fila++) {
            for (int col = 0; col < COLUMNAS; col++) {
                if (casillas[fila][col].getFicha() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean hayGanador(Ficha ficha) {
        // Comprobar si hay una línea horizontal de 4 fichas del mismo color
        for (int fila = 0; fila < FILAS; fila++) {
            int contador = 0;
            for (int col = 0; col < COLUMNAS; col++) {
                if (casillas[fila][col].getFicha() != null && 
                    casillas[fila][col].getFicha().getColor().equals(ficha.getColor())) {
                    contador++;
                    if (contador == 4) {
                        return true;
                    }
                } else {
                    contador = 0;
                }
            }
        }
    
        // Comprobar si hay una línea vertical de 4 fichas del mismo color
        for (int col = 0; col < COLUMNAS; col++) {
            int contador = 0;
            for (int fila = 0; fila < FILAS; fila++) {
                if (casillas[fila][col].getFicha() != null && 
                    casillas[fila][col].getFicha().getColor().equals(ficha.getColor())) {
                    contador++;
                    if (contador == 4) {
                        return true;
                    }
                } else {
                    contador = 0;
                }
            }
        }
    
        // Comprobar si hay una línea diagonal hacia la derecha de 4 fichas del mismo color
        for (int fila = 0; fila < FILAS - 3; fila++) {
            for (int col = 0; col < COLUMNAS - 3; col++) {
                if (casillas[fila][col].getFicha() != null &&
                    casillas[fila][col].getFicha().getColor().equals(ficha.getColor()) &&
                    casillas[fila + 1][col + 1].getFicha() != null &&
                    casillas[fila + 1][col + 1].getFicha().getColor().equals(ficha.getColor()) &&
                    casillas[fila + 2][col + 2].getFicha() != null &&
                    casillas[fila + 2][col + 2].getFicha().getColor().equals(ficha.getColor()) &&
                    casillas[fila + 3][col + 3].getFicha() != null &&
                    casillas[fila + 3][col + 3].getFicha().getColor().equals(ficha.getColor())) {
                    return true;
                }
            }
        }
    
        // Comprobar si hay una línea diagonal hacia la izquierda de 4 fichas del mismo color
        for (int fila = 0; fila < FILAS - 3; fila++) {
            for (int col = 3; col < COLUMNAS; col++) {
                if (casillas[fila][col].getFicha() != null &&
                casillas[fila][col].getFicha().getColor().equals(ficha.getColor()) &&
                casillas[fila + 1][col - 1].getFicha() != null &&
                casillas[fila + 1][col - 1].getFicha().getColor().equals(ficha.getColor()) &&
                casillas[fila + 2][col - 2].getFicha() != null &&
                casillas[fila + 2][col - 2].getFicha().getColor().equals(ficha.getColor()) &&
                casillas[fila + 3][col - 3].getFicha() != null &&
                casillas[fila + 3][col - 3].getFicha().getColor().equals(ficha.getColor())) {
                return true;
            }
            }
        }
    
        // No se ha encontrado una línea de 4 fichas del mismo color
        return false;
    }




    
    


}


