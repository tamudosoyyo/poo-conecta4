public class ColumnaLlenaException extends Exception {
    public ColumnaLlenaException(String message) {
        super(message);
    }
}
