import java.util.Scanner;

public class JugadorMaquina extends Jugador {
    public JugadorMaquina(String color, String nombre) {
        super(color,nombre);
    }

    @Override 
    public void jugarTurno(ControladorJuego controlador, Scanner sc){
        int columnaElegida = elegirColumna(controlador.getJuego().getTablero());
        try {
            controlador.realizarJugada(columnaElegida);
        } catch (Exception e) {
            System.out.println("Ha ocurrido un error inesperado: " + e.getMessage());
        }
    };

    private int elegirColumna(Tablero tablero) {
        for (int columna = 0; columna < 7; columna++) {
            if (!tablero.columnaLlena(columna)) {
                return columna;
            }
        }
        return -1; 
    }
}

